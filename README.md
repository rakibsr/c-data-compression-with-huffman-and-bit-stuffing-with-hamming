# README #
This is a C based project and includes standard C libraries. It includes following files-
main.h: Public function signature which can be accessed through compiled dll
shrhelp.h: Preprocessors needed for dll compilation
main.c: Function defines including main().

It has been compiled with MS Visual Studio 2015. But it should work with any standard C compiler.
Just include those file in your C project and compile.


### What is this repository for? ###

This repo is based on the project for M. Engg. in ICT. "Design of an FPGA Based System for High Speed Data Compression and Secured Transmission". 
Actual project has been implemented Verilog HDL. Initially it has been developed using C as a proof of concept.

To secure text data for transmission, Hamming (7, 4) coding is used. It secures data by adding a layer of encryption as well as by providing 
facility for error detection and correction for reliable transmission. In this step, each character of sending text data is divided into groups 
of 4-bit data and encoded with 3 parity bits, thus resulting 7-bit code-word. This system can correct 2-bit errors in each 8-bit character. 
Furthermore, the system includes a two-level compression. During the first level of compression, redundant bits of 8 bit characters are removed 
which has been mentioned as bit-stuffing. After compressing by bit-stuffing, second level compression is done by Huffman algorithm. These two 
level compression processes can achieve higher saving percentage of memory. Hence, the system provides reliability in transmission and also 
compresses data to larger extent. The results obtained are highly promising and the system is very effective for providing high level reliability 
and higher saving percentage of memory which in turn reduces bandwidth and transmission time.

### How do I get set up? ###
Check main(). You will get the idea. "input*.txt" contain input data set.

### Contribution guidelines ###
This project is licensed under the MIT License

### Acknowledgement ###
http://left404.com/simplehuffman/
http://michael.dipperstein.com/hamming/

### Who do I talk to? ###
Rakib Mohammad