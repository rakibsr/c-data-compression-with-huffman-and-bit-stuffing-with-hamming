#include "main.h"
#include "shrhelp.h"

#define EXPORT_FCNS

// ************************* Utils

/* Uses
char *content; 
if (!readTxtFile("input.txt", &content)) {
//error
}
*/
int readTxtFile(char *fileName, char **dataRead)
{
    FILE *fp;
    long size= 0;
    fp= fopen(fileName, "r+");
    if (!fp) return 0;

    fseek(fp, 0, SEEK_END);
    size= ftell(fp);
    fseek(fp,0, SEEK_SET);
    *dataRead= (char *)malloc(size + 1);
    if (size != fread(*dataRead, sizeof(char), size, fp)) 
    { 
        free(*dataRead);
        return 0; // -2 means file reading fail 
    } 
    fclose(fp);
    (*dataRead)[size] = 0;
    return size;
}

int writeTxtFile(char *fileName, char *dataWrite)
{
    FILE *fp= fopen(fileName, "w+");
    if (!fp) return 0; // failed

    fputs(dataWrite, fp);
    fclose(fp);
    return 1; //successful

}

int writeBinaryFile(const char *fileName, char *dataToWrite)
{
  FILE *fp= fopen(fileName, "wb");
  if (!dataToWrite || !fileName) return 0; //failed
  fwrite(dataToWrite, sizeof(char), strlen(dataToWrite), fp);
  fclose(fp);
  return 1;

  /*
  struct rec
  {
    int x,y,z;
  };

  int main()
  {
    int counter;
    FILE *ptr_myfile;
    struct rec my_record;

    ptr_myfile=fopen("test.bin","wb");
    if (!ptr_myfile)
    {
      printf("Unable to open file!");
      return 1;
    }
    for ( counter=1; counter <= 10; counter++)
    {
      my_record.x= counter;
      fwrite(&my_record, sizeof(struct rec), 1, ptr_myfile);
    }
    fclose(ptr_myfile);
    return 0;
  }
   */
}

long FileSize(char *in)
{
    FILE *pInFile;
    long size= 0;

    pInFile = fopen(in, "rb+");
    fseek(pInFile, 0, SEEK_END); //move to end
    size= ftell(pInFile);
    fseek(pInFile,0, SEEK_SET); //move to start
    fclose(pInFile);
    return size;
}

double CR(char *inFileName, char *compressedFileName)
{
    return ((double)FileSize(compressedFileName) / FileSize(inFileName)) * 100;
}

// CR in 100%
double SavingPercentage(double CR)
{
    return (100 - CR);
}

// *********************** Hamming Code
/***************************************************************************
Function   : SumBitsModulo2
Description: This function performs a modulo 2 sum of the least
             significant 7 bits in an unsigned char.
Parameters : bits - right justified array of 7 bits to be summed modulo 2.

Returned   : 0 if even number of bits set to 1.
             1 if odd number of bits set to 1.

Modulo 2 addition works as follows:
0 + 0 = 0
0 + 1 = 1
1 + 0 = 1
1 + 1 = 0
A + B = B + A
A + B + C = (A + B) + C = A + (B + C)

Modulo 2 multiplication works as follows:
0 x 0 = 0
0 x 1 = 0
1 x 0 = 0
1 x 1 = 1
A x B = B x A
A x B x C = (A x B) x C = A x (B x C)

Notice that modulo 2 addition functions just like a logical Exclusive OR (XOR) and modulo 2 multiplication functions just like a logical AND.
***************************************************************************/
unsigned char SumBitsModulo2(unsigned char bits)
{
    unsigned char sum, mask;

    sum = 0;

    for (mask = 0x01; mask < (0x01 << CODE_BITS); mask <<=1 )//mask will have values 1,2,4,8,16,32,64
    {
        if (bits & mask)
        {
            sum++;
        }
    }

    return (sum & 0x01);
}

/***************************************************************************
Function   : HammingMatrixEncode
Description: This function uses a generator matrix G to determine the Hamming code for a DATA_BITS(4 bit) long value.
             Actually, G Transpose is used in the calculation, because it makes the C code a little easier.
             Notice that '&' is used for modulo 2 multiplication.
Parameters : data - 4 long series of data bits to encode.
Returned   : Hamming code used to represent data which is a 7 bit codeword

Basic:
Hamming code(7,4): Data bit length= 4 and parity bit length= 3. Total codeword 7 bit.
Parity bit will be added in bit position 1,2 and 4.
Bit positions
(7)   (6)   (5)   (4)   (3)   (2)    (1)

Parity is calculated as following rule
Parity bit P1 = XOR of bits (3,5,7)
Parity bit P2 = XOR of bits (3,6,7)
Parity bit P4 = XOR of bits (5,6,7)

Other bit position(3,5,6,7) will be filled up by original data bits.
Hamming Code Generation using Matrix:
Hamming code(7,4): Data bit length= 4 and parity bit length= 3. Total length of codeword 7 bit.
Parity bits will be added in bit positions 7,6,5.

Bit positions in resulting codeword
7(MSB)  6      5      4      3      2     1(LSB)
(p1)   (p2)   (p3)   (d1)   (d2)   (d3)   (d4)

Given: data bits d1, d2, d3, and d4
A (7, 4) Hamming code may define parity bits p1, p2, and p3 as
p1 = d2 + d3 + d4,  Why? becuase p1 is 7th bit position in codeword. 7=4+2+1. 4th bit position is d1, 2nd bit position is d3
p2 = d1 + d3 + d4
p3 = d1 + d2 + d4

So a codeword will be [p1, p2, p3, d1, d2, d3, d4]

To convert 4 bit data into 7 bit code word we can use a 4x7 generator matrix [G].
Modulo 2 multiplication of d and [G] (d.[G]) is the desired 1x7 Hamming code word. 

Define data matrix, d to be the 1x4 vector [d1 d2 d3 d4]

Creating G in below steps:
Step 1. Put 1 in each column according to data bit position. 
so for d1, put 1 in first row.
for d2, put 1 in second row.
         1 
d1 =     0 
         0 
         0 
-------------------
         0 
d2 =     1 
         0 
         0 
-------------------
         0 
d3 =     0 
         1 
         0 
-------------------
         0 
d4 =     0 
         0 
         1 
-------------------

Step 2. Put 1 in the rows corresponding to each data bit positions included in 
the computation of parity bit and a zero in all other rows. 
For example p1 = d2 + d3 + d4. So 1 will be put into row positions 2,3 and 4.
        0 
p1 =    1 
        1 
        1 
-----------------
        1 
p2 =    0 
        1 
        1 
----------------
        1 
p3 =    1 
        0 
        1 
----------------
Step 3. To create a 4x7 generator matrix,G that produces codewords with the bits ordered 
p1, p2, p3, d1, d2, d3, d4 (3 parity bits followed by 4 data bits) use 
the vectors from the previous steps[ds and ps] and arrange them into the following 
columns [p1 p2 p3 d1 d2 d3 d4]

The results are following 4x7 generator matrix:

     p1    p2   p3   d1   d2   d3   d4
     0     1    1    1    0    0    0 
G =  1     0    1    0    1    0    0 
     1     1    0    0    0    1    0 
     1     1    1    0    0    0    1 

Arranging the columns in any other order will just change the positions of bits in the 
code word.

Example:
Encode the data value 1010(1x4 matrix) using the Hamming code defined by the matrix G (4x7).

                     0    1    1    1    0    0    0      
     |1 0 1 0|       1    0    1    0    1    0    0     =   1 0 1 1 0 1 0                                        
                     1    1    0    0    0    1    0         | | | | | | |
                     1    1    1    0    0    0    1         | | | | | | +-->(1 x 0) + (0 x 0) + (1 x 0) + (0 x 1)
                                                             | | | | | +---->(1 x 0) + (0 x 0) + (1 x 1) + (0 x 0)
                                                             | | | | +------>(1 x 0) + (0 x 1) + (1 x 0) + (0 x 0)
                                                             | | | +-------->(1 x 1) + (0 x 0) + (1 x 0) + (0 x 0)
                                                             | | +---------->(1 x 1) + (0 x 1) + (1 x 0) + (0 x 1)
                                                             | +------------>(1 x 1) + (0 x 0) + (1 x 1) + (0 x 1)
                                                             +-------------->(1 x 0) + (0 x 1) + (1 x 1) + (0 x 1)

So 1010 encodes to 1011010. Equivalent Hamming codes represented by different generator matrices will produce different results.

Matrix multiplication is done by modulo-2 addition and multiplication.
If [A] and [B] are matrices [A] x [B] = [R] is possible when
[A] is an ixj matrix (i rows and j columns) and 
[B] is a  jxk matrix (j rows and k columns).
and [R] will be a ixk matrix.
***************************************************************************/
unsigned char HammingMatrixEncode(unsigned char data)
{
    unsigned char i, code;
    unsigned char rowBits;

    code = 0;

    for (i = 0; i < 7; i++) //there are 7 columns in G. We have to multiply 1x4 data matrix to 4x7 generator matrix
    {
        code <<= 1;     /* code bits are accumlated from right to left */ //instead of writing x = x << 1;, you can write x <<= 1;.

        /* each component of data row times column c[i] */
        rowBits = (gT[i] & data);       /* mod 2 multiplication of vectors */

        /* determine modulo 2 sum of compoments */
        if (SumBitsModulo2(rowBits))
        {
            code |= 1; //insert 1 if modulo sum is 1 in each operation.
        }
    }

    return code;
}

/***************************************************************************
*   Function   : HammingMatrixDecode
*   Description: This function uses the matrix H (above) to determine the
*                value encoded by a 7 long code.  H is a parity
*                check matrix based on the encoding matrix G.  The result
*                of multiplying the codeword by H is called the syndrome.  If
*                there are no errors in the code, the syndrome will be a 0
*                vector.  If the syndrome is not 0, it will match a column
*                in H.  The column it matches is likely the errored bit.
*                Toggle the errored bit and the resulting code is the
*                nearest matching correct code.
*   Parameters : code - 7 bit codeword to decode.
*   Effects    : None
*   Returned   : Nearest value to encoded data

Basic:
Received codeword will be as [D7 D6 D5 P4 D3 P2 P1]
 When receiver receives data, he will calculate check bits as
�         C1 = XOR of bits (1, 3, 5, 7)
�         C2 = XOR of bits (2, 3, 6, 7)
�         C4 = XOR of bits (4, 5, 6, 7)
Now receiver will create number with check bits to find error position as    C4C2C1 (Not ANDING)
If C4C2C1 = 000 that means no error
If C4C2C1 = 001 that means error in first bit. So flip it.
If C4C2C1 = 101 that means error in 5th bit because 101 is binary of 5

Using Matrix:
Parity may also be validated using matrix operations. A 3x7 parity check matrix [H] may be constructed such that row 1 contains 1s in the position 
of the first parity bit and all of the data bits that are included in its parity calculation. Row 2 contains 1s in the position of the second parity 
bit and all of the data bits that are included in its parity calculation. Row 3 contains 1s in the position of the third parity bit and all of the 
data bits that are included in its parity calculation.

Example:
Using the code from example above, the matrix H may be defined as follows:
        p1   p2   p3   d1   d2   d3   d4
        1    0    0    0    1    1    1 
H =     0    1    0    1    0    1    1 
        0    0    1    1    1    0    1 

Multiplying the 3x7 matrix [H] by a 7x1 matrix representing the encoded data produces a 3x1 matrix called the "syndrome". There are two useful 
proprieties of the syndrome. 
If the syndrome is all zeros, the encoded data is error free. 
If the syndrome has a non-zero value, flipping the encoded bit that is in the position of the column in [H] that matches the syndrome will result in a valid code word.

Example:
Using the parity check matrix from the example above we can correct and verify the codeword 1011011.

                                                 1
                                                 0
 |    1    0    0    0    1    1    1 |          1          (1 x 1) + (0 x 0) + (0 x 1) + (0 x 1) + (1 x 0) + (1 x 1) + (1 x 1)           |1|
 |    0    1    0    1    0    1    1 |    X     1     =    (0 x 0) + (1 x 0) + (0 x 1) + (1 x 1) + (0 x 0) + (1 x 1) + (1 x 1)     =     |1|
 |    0    0    1    1    1    0    1 |          0          (0 x 1) + (0 x 0) + (1 x 1) + (1 x 1) + (1 x 0) + (0 x 1) + (1 x 1)           |1|
                                                 1
                                                 1

There is a parity error. Looking back at the matrix [H], you will see that the seventh column is all 1s, so the seventh bit is the errored bit. 
Changing the seventh bit produces the code word 1011010.

                                               1
                                               0
     1    0    0    0    1    1    1           1          (1 x 1) + (0 x 0) + (0 x 1) + (0 x 1) + (1 x 0) + (1 x 1) + (1 x 0)          |0|
     0    1    0    1    0    1    1     X     1     =    (0 x 0) + (1 x 0) + (0 x 1) + (1 x 1) + (0 x 0) + (1 x 1) + (1 x 0)     =    |0|
     0    0    1    1    1    0    1           0          (0 x 1) + (0 x 0) + (1 x 1) + (1 x 1) + (1 x 0) + (0 x 1) + (1 x 0)          |0|
                                               1      
                                               0      
Sure enough 1011010 is a valid code word. As I stated at the top of this section remove the parity bits to get the encoded value. 
In this case 1011011 was likely transmitted as 1011010, which encodes 1010.
***************************************************************************/
unsigned char HammingMatrixDecode(unsigned char code)
{
    unsigned char i, syndromeVal;
    unsigned char syndromeColBits;  /* sum of bits is bit in syndrome */

    syndromeVal = 0;

    for (i = 0; i < 3; i++)//we multiply 3x7 parity checker matrix to 7x1 codeword matrix
    {
        syndromeVal <<= 1;

        /* components of row h[i] times column code */
        syndromeColBits = (h[i] & code);

        /* determine modulo 2 sum of compoments */
        if (SumBitsModulo2(syndromeColBits))
        {
            syndromeVal |= 1;
        }
    }

    /* return the data corrected for error */
    return ((code ^ syndromeMask[syndromeVal]) & (0xFF >> DATA_BITS));//return ((code ^ syndromeMask[syndromeVal]) & 0x0F);
}

void encryptFileWithHamming(char *inputFileName, char *encryptedFileName)
{
    char aByteToEncode= 0;
    unsigned char newByteOne= 0;
    unsigned char newByteTwo= 0;
    unsigned char h1= 0;
    unsigned char h2= 0;
    FILE *inFile, *outFile;
	int fileSize= 0;
	int i;

	if (!inputFileName || !encryptedFileName) return;

    // Now open input and output file
    inFile = fopen(inputFileName, "rb+");
    outFile = fopen(encryptedFileName,"wb+");

	fileSize= FileSize(inputFileName);

    for (i= 0; i < fileSize; i++) {
		aByteToEncode = fgetc(inFile);
        // 3. devide into two byte. bit 1~4 and 5~8
        newByteOne= 0x0F & aByteToEncode;
        newByteTwo= 0xF0 & aByteToEncode;

        // 4. Each divided character part into new byte in position 1~4
        newByteTwo= newByteTwo >> 4; // newByteOne is already in 1~4 position.

        // 5. do hamming each two new byte -> h1 and h2
        h1= HammingMatrixEncode(newByteOne);
        h2= HammingMatrixEncode(newByteTwo);

        // Handle null character
        if (h1 == 0x00) {
            //h1= 0x80;
			//printf("--> NULL encountered in encryption\n");
        }
        if (h2 == 0x00) {
            //h2= 0x80;
			//printf("--> NULL encountered in encryption\n");
        }
		fputc(h1, outFile);
		fputc(h2, outFile);
    }
	// Remember to close file stream
    fclose(inFile);
    fclose(outFile);
}

void decryptFileWithHamming(char *encryptedFileName, char *decryptedFileName)
{
    char decodedChar= 0;
    unsigned char newByteOne= 0;
    unsigned char newByteTwo= 0;
    unsigned char h1= 0;
    unsigned char h2= 0;
    char charOne= 0;
    char charTwo= 0;
    FILE *inFile, *outFile;
	int fileSize= 0;
	int i;

    if (!encryptedFileName) return;

	// Now open input and output file
    inFile = fopen(encryptedFileName, "rb+");
    outFile = fopen(decryptedFileName,"wb+");

	fileSize= FileSize(encryptedFileName);

   for (i= 0; i < fileSize; i+= 2) {
		charOne = fgetc(inFile);
		charTwo= fgetc(inFile);
        //Refresh
        decodedChar= 0;

        // Handle null character
        //if (charOne == 0x80) charOne= 0x00;
        //if (charTwo == 0x80) charTwo= 0x00;

        h1= HammingMatrixDecode(charOne);
        h2= HammingMatrixDecode(charTwo);

        // Making sure that upper 4 bits are 0
        h1&= 0x0F;
        h2&= 0x0F;

        // Making h2 as upper 4 bit as it should be.
        h2= h2 << 4;

        // Getting original character h2h1
        decodedChar|= h1;
        decodedChar|= h2;

        // Save the original character
		fputc(decodedChar, outFile);
    }
    // Remember to close file stream
    fclose(inFile);
    fclose(outFile);
}


// ********************** HUFFMAN
/*
Steps to build Huffman Tree
Input is array of unique characters along with their frequency of occurrences and output is Huffman Tree.

1. Create a leaf node for each unique character and build a min heap of all leaf nodes (Min Heap is used as a priority queue. The value of frequency field 
is used to compare two nodes in min heap. Initially, the least frequent character is at root)
2. Extract two nodes with the minimum frequency from the min heap.
3. Create a new internal node with frequency equal to the sum of the two nodes frequencies. Make the first extracted node as its left child and the other 
extracted node as its right child. Add this node to the min heap.
4. Repeat steps#2 and #3 until the heap contains only one node. The remaining node is the root node and the tree is complete.

Steps to print codes from Huffman Tree:
Traverse the tree formed starting from the root. Maintain an auxiliary array. While moving to the left child, write 0 to the array. While moving to the 
right child, write 1 to the array. Print the array when a leaf node is encountered.
*/

/* print specified error message and quite */
void Error(const char *msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

/* deallocate given Huffman tree */
void FreeTree(htree *tree)
{
    if(tree)
    {
        FreeTree(tree->left);
        FreeTree(tree->right);
        free(tree);
    }
}

/* deallocate table of Huffman encodings */
void FreeTable(char *table[])
{
    int i;
    for(i = 0; i < CHAR_RANGE; i++) if(table[i]) free(table[i]);
}


// ****************** Compression

/* compare two Huffman trees based on frequency, descending order */
int CmpTrees(const void *a, const void *b)
{
    const htree **x = (const htree **) a, **y = (const htree **) b;
    if((*x)->freq == (*y)->freq) return 0;
    else return ((*x)->freq < (*y)->freq) ? 1 : -1;
}

/* create a new string with given letter concatenated on to the prefix */
char *Concat(char *prefix, char letter)
{
    char *result = (char *)malloc(strlen(prefix) + 2);
    sprintf(result, "%s%c", prefix, letter);
    return result;
}

/* build and return a Huffman tree based on a frequency table */
htree *BuildTree(int frequencies[])
{
    int i, len = 0;
    htree *queue[CHAR_RANGE];
    
    /* create trees for each character, add to the queue */
    for(i = 0; i < CHAR_RANGE; i++)
    {
        if(frequencies[i])
        {
            htree *toadd = (htree *)calloc(1, sizeof(htree));
            toadd->letter = i;
            toadd->freq = frequencies[i];

            queue[len++] = toadd;
        }
    }
    
    while(len > 1)
    {
        htree *toadd = (htree *)malloc(sizeof(htree));
        
        /* sort - smallest frequency trees are last ie if a < b then sort will be descending 55 34 14 2*/
        qsort(queue, len, sizeof(htree *), CmpTrees);
        
        /* dequeue two lowest frequency trees, build new tree from them */
        toadd->left = queue[--len];
        toadd->right = queue[--len];
        toadd->freq = toadd->left->freq + toadd->right->freq;
        
        queue[len++] = toadd; /* insert back in the queue */
    }
    
    return queue[0]; /* last tree in the queue is the full Huffman tree */
}

/* traverse the Huffman tree to build up a table of encodings */
void TraverseTree(htree *tree, char **table, char *prefix)
{
    if(!tree->left && !tree->right) table[tree->letter] = prefix;
    else
    {
        if(tree->left) TraverseTree(tree->left, table, Concat(prefix, '0'));
        if(tree-> right) TraverseTree(tree->right, table, Concat(prefix, '1'));
        free(prefix);
    }
}

/* build a table of Huffman encodings given a set of frequencies */
char **BuildTable(int frequencies[])
{
    static char *table[CHAR_RANGE];
    char *prefix = (char *)calloc(1, sizeof(char));
    htree *tree = BuildTree(frequencies);
    TraverseTree(tree, table, prefix);
    FreeTree(tree);
    
    return table;
}


/* output the Huffman header for an encoded file */
void WriteHeader(FILE *out, int frequencies[])
{
    int i, count = 0;
    
    for(i = 0; i < CHAR_RANGE; i++) if(frequencies[i]) count++;
    fprintf(out, "%d\n", count);
    
    for(i = 0; i < CHAR_RANGE; i++)
        if(frequencies[i]) fprintf(out, "%d %d\n", i, frequencies[i]);
}

/* write the given bit encoding to the output file */
void WriteBits(const char *encoding, FILE *out)
{
    // static used to initialize them just for once. We need to retain their values as long bitcount is not 8.
    static int bits = 0;  //raw bits to be write into file. When 1 byte information available it will be write into file
    static int bitcount = 0; //Number of bit written on "bits" buffer.
    
    while(*encoding)
    {
        /* push bits on from the right */
        //bit encoding pushed into "bits" buffer from right to left. Shift previously written bits into left.
        // assume bits=0000 0010 (2). Now bit encoding "01" will be stored into "bits" buffer
        // *encoding = 0, bits = 2 * 2 + 48 -48 = 4 = 0000 0100; So *encoding=0 is stored at "bits" buffer
        // *encoding = 1, bits = 4 * 2 + 49 -48 = 9 = 0000 1001; So *encoding=1 is stored at "bits" buffer
        bits = bits * 2 + *encoding - '0'; // ascii value of compressed character to be written on file.
        bitcount++;
        
        /* when we have filled the char, output as a single character */
        if(bitcount == CHAR_BITS)
        {
            fputc(bits, out);
            bits = 0;
            bitcount = 0;
        }
        
        encoding++;
    }
}

/* create a Huffman encoding for the file in and save the encoded version to out */
void HuffmanCompression(char *inFileName, char *outFileName)
{
    FILE *inputFile, *outputCompressedFile, *headerFile;
    int c, frequencies[CHAR_RANGE] = { 0 };
    char **table;

    // Now open input,output and header file
    inputFile = fopen(inFileName, "rb+");
    outputCompressedFile = fopen(outFileName,"wb+");
    headerFile = fopen("header","wb+");
    
    while((c = fgetc(inputFile)) != EOF) frequencies[c]++;
    
    frequencies[FAKE_EOF] = 1;
    rewind(inputFile); //sets the file position to the beginning of the file of the given stream.
    
    table = BuildTable(frequencies);
    WriteHeader(headerFile, frequencies); //Write frequency information in seperate file for decompression purpose
    
    while((c = fgetc(inputFile)) != EOF)
        WriteBits(table[c], outputCompressedFile);
    
    /* use FAKE_EOF to indicate end of input */
    WriteBits(table[FAKE_EOF], outputCompressedFile);
    
    /* write an extra 8 blank bits to flush the output buffer */
    WriteBits("0000000", outputCompressedFile);
    
    FreeTable(table);
    // Remember to close file stream
    fclose(inputFile);
    fclose(outputCompressedFile);
    fclose(headerFile);
}


// ================= DECOMPRESSION

/* read in the header of a Huffman encoded file */
int *ReadHeader(FILE *in)
{
    static int frequencies[CHAR_RANGE];
    int i, count, letter, freq;
    
    if(fscanf(in, "%d", &count) != 1) Error("invalid input file.");
    
    for(i = 0; i < count; i++)
    {
        if((fscanf(in, "%d %d", &letter, &freq) != 2)
           || letter < 0 || letter >= CHAR_RANGE) Error("invalid input file.");
        
        frequencies[letter] = freq;
    }
    fgetc(in); /* discard last newline */
    
    return frequencies;
}

/* read a single bit from the input file */
int ReadBit(FILE *in)
{
    /* buffer holding raw bits and size of MSB filled */
    static int bits = 0, bitcount = 0; //initialize once
    int nextbit;
    
    if(bitcount == 0)
    {
        bits = fgetc(in);
        bitcount = 128;//(1 << (CHAR_BITS - 1)); // initialize to 128. MSB is not used for ascii char
    }
    
    nextbit = bits / bitcount; //Get the next bit 7th,6th,5th....
    bits %= bitcount;
    bitcount /= 2; //64,32,16,8,4,2,1,0
    
    return nextbit;
}

/* decode and return a single character from the input using the given Huffman
 * tree */
int DecodeChar(FILE *in, htree *tree)
{
    while(tree->left || tree->right)
    {
        if(ReadBit(in)) tree = tree->right;
        else tree = tree->left;
        
        if(!tree) Error("invalid input file.");
    }
    return tree->letter;
}

/* decode the Huffman-encoded file in and save the results to out */
void HuffmanDecompression(char *pInFileName, char *pOutFileName)
{
    int *frequencies, c;
    htree *tree;
    FILE *pInputCompressedFile, *pOutputFile, *pHeaderFile;

    // Now open input, output and header file
    pInputCompressedFile = fopen(pInFileName, "rb+");
    pOutputFile = fopen(pOutFileName,"wb+");
    pHeaderFile = fopen("header", "rb+");
    
    frequencies = ReadHeader(pHeaderFile);
    tree = BuildTree(frequencies);
    
    while((c = DecodeChar(pInputCompressedFile, tree)) != FAKE_EOF)
        fputc(c, pOutputFile);
    
    FreeTree(tree);
    // Remember to close file stream
    fclose(pInputCompressedFile);
    fclose(pOutputFile);
    fclose(pHeaderFile);
}



// *********************** Bit Stuffing
/* Reduce file size by */
void BitCompression(char *pInFileName, char *pCompressedFileName)
{
    FILE *inputTxtFile, *outputCompressedFile;
    // bit compression
    long fileSize=0;
    long multiple= 0;
    long remainingChars= 0;
    int c= 0;

    fileSize= FileSize(pInFileName);
    multiple= fileSize / 8;
    remainingChars= fileSize % 8;

    // Now open input and output file
    inputTxtFile = fopen(pInFileName, "rb+");
    outputCompressedFile = fopen(pCompressedFileName,"wb+");

    while(multiple) {
        unsigned char ic[8];
        int i;
        for (i=0; i<8; i++) {
            ic[i]= fgetc(inputTxtFile);
        }
        // replace MSB of first 7 characters by bits of 8th character
        for (i=1; i<=7; i++) {
            unsigned char oc;
            unsigned char eighthByte= (ic[7] << i) & 0x80; //Get desired bit of 8th character into MSB position. eightByte is now X000 0000
            oc= ic[i-1] | eighthByte; //Replace MSB of i-th character by i-th bit of eighth character, first position is counted from left
            fputc(oc, outputCompressedFile);
        }
        multiple--;
    }
    
    // Lets write down remaining character into output compressed file
    while((c= fgetc(inputTxtFile)) != EOF) fputc(c, outputCompressedFile);
    //frequencies[FAKE_EOF] = 1;

    // Remember to close file stream
    fclose(inputTxtFile);
    fclose(outputCompressedFile);
}

void BitDecompression(char *pInFileName, char *pOutFileName)
{
    FILE *pInputCompressedFile, *pOutputFile;
    long fileSize= 0;
    long multiple= 0;
    long remainingChars= 0;
    int c= 0;

    fileSize= FileSize(pInFileName);
    multiple= fileSize / 7;
    remainingChars= fileSize % 7;

    // Now open input and output file
    pInputCompressedFile = fopen(pInFileName, "rb+");
    pOutputFile = fopen(pOutFileName,"wb+");

    while(multiple) {
        unsigned char ic[7];
        unsigned char eighthChar= 0;
        unsigned char oc;
        unsigned char temp= 0;
        int i;
        for (i=0; i<7; i++) {
            ic[i]= fgetc(pInputCompressedFile);
        }
        // Decompress first 7 byte to construct 8 byte
        for (i=1; i<=7; i++) {
            oc= ic[i-1] & 0x7F; //Remove stored bit of 8th character from MSB
            fputc(oc, pOutputFile);
            
            temp= (ic[i-1] >> i) & (0x80 >> i); //Move MSB of i-th character to i-th position. temp is now 0X00 0000, 00X0 0000, 000X 0000 ....
            eighthChar|= temp; //Restore bit value in 8th character from i-th character.
        }
        fputc(eighthChar, pOutputFile);
        multiple--;
    }
    
    // Lets write down remaining character into output decompressed file
    while((c= fgetc(pInputCompressedFile)) != EOF) fputc(c, pOutputFile);
    //frequencies[FAKE_EOF] = 1;

    // Remember to close file stream
    fclose(pInputCompressedFile);
    fclose(pOutputFile);
}


// **************************** Main Algorithm
// 1. Compress 2. Encode

void CompressionWithBitStuffingAndHuffman(char *pInFileName, char *pCompressedFileName)
{
    BitCompression(pInFileName, "temp");
    HuffmanCompression("temp", pCompressedFileName);
    remove("temp");
}

void DecompressionWithBitStuffingAndHuffman(char *pInFileName, char *pOutFileName)
{
    HuffmanDecompression(pInFileName, "temp2");
    BitDecompression("temp2", pOutFileName);
    remove("temp2");
}

int main()
{
	double cr;
	/******* Test Hamming ***********/
	encryptFileWithHamming("input.txt", "hamming");
	decryptFileWithHamming("hamming", "out.txt");


	/************* Test Bitstuffing ************/
	//// bit compression
	//BitCompress("input.txt", "compressed");
	//printf("***Bit compression completed!***\n\n");
	//printf("Input File Size [%ld]\n", FileSize("input.txt"));
	//printf("Compressed File Size [%ld]\n", FileSize("compressed"));
	//printf("Compression Ratio [%.2lf]\n", CR("input.txt", "compressed"));

	//// bit decompression
	//BitDecompress("compressed", "output.txt");
	//printf("Bit Decompression completed!\n");


	/************* Test Huffman ************/
	//// compression
	//HuffmanCompression("input.txt", "compressed");
	//printf("***Huffman compression completed!***\n\n");
	//printf("Input File Size [%ld]\n", FileSize("input.txt"));
	//printf("Compressed File Size [%ld]\n", FileSize("compressed"));
	//printf("Compression Ratio [%.2lf]\n", CR("input.txt", "compressed"));

	//// decompression
	//HuffmanDecompression("compressed", "output.txt");
	//printf("Huffman Decompression completed!\n");

	/*************** Test Bit stuffing + Huffman **********************/
	//CompressionWithBitStuffingAndHuffman("input.txt","compressed");
	//printf("***Compression completed!***\n\n");
	//printf("Input File Size [%ld]\n", FileSize("input.txt"));
	//printf("Compressed File Size [%ld]\n", FileSize("compressed"));
	//printf("Compression Ratio: %.2lf%\n", CR("input.txt", "compressed"));

	//DecompressionWithBitStuffingAndHuffman("compressed", "output.txt");
	//printf("Output File Size [%ld]\n", FileSize("output.txt"));
	//printf("Decompression completed!\n");


	/*####################### MAIN algorithm test ######################## */
	// 1. Compress 2. Encrypt
	CompressionWithBitStuffingAndHuffman("input.txt", "compressed");
	printf("***Compression completed!***\n\n");
	printf("Input File Size [%ld]\n", FileSize("input.txt"));
	printf("Compressed File Size [%ld]\n", FileSize("compressed"));
	cr = CR("input.txt", "compressed");
	printf("Compression Ratio: %.2lf%\n", cr);
	printf("Saving Percentage: %.2lf%\n", SavingPercentage(cr));

	encryptFileWithHamming("compressed", "encrypt");
	printf("***Encryption completed!***\n\n");


	// 1. Decrtypt 2. Decompress
	decryptFileWithHamming("encrypt", "decrypt");
	printf("***Decryption completed!***\n\n");

	DecompressionWithBitStuffingAndHuffman("decrypt", "output.txt");
	printf("Output File Size [%ld]\n", FileSize("output.txt"));
	printf("Decompression completed!\n");


	return 0;
}