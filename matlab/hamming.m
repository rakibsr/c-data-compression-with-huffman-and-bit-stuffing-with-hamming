%%  Functions:
% CompressionWithBitStuffingAndHuffman(char *pInFileName, char *pCompressedFileName);
% encryptFileWithHamming(char *inputFileName, char *encryptedFileName);
% decryptFileWithHamming(char *encryptedFileName, char *decryptedFileName);
% DecompressionWithBitStuffingAndHuffman(char *pInFileName, char *pOutFileName);
% long FileSize(char *in);
% double CR(char *inFileName, char *compressedFileName);
% double SavingPercentage(double CR);

% dll.dll
%fprintf('%s will be %d this year.\n',name,age);
%delete filename
%calllib('bitdll', 'encryptFile', 'input.txt', 'encrypt')
%calllib('bitdll', 'decryptFile', 'encrypt', 'decrypted.txt')


%%%%%%%%%%%%%%%%%% input1.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
fprintf('*** Running algorithm for dataset 1 (input1.txt) ***\n')
inputSize1= calllib('dll', 'FileSize', 'input1.txt');
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input1.txt', 'compressed1')
fprintf('*** Compression Completed!!!\n');
fprintf('Input File Size: %d Bytes\n', inputSize1);
compressedSize1= calllib('dll', 'FileSize', 'compressed1');
fprintf('Compressed File Size: %d Bytes\n', compressedSize1);
cr1= calllib('dll', 'CR', 'input1.txt', 'compressed1');
sp1= calllib('dll', 'SavingPercentage', cr1);
fprintf('Compression Ratio: %.2f\n', cr1);
fprintf('Saving Percentage: %.2f\n', sp1);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed1', 'encrypted1')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted1', 'decrypted1')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted1', 'output1.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize1= calllib('dll', 'FileSize', 'output1.txt');
fprintf('Output File Size: %d Bytes\n', outputSize1);
delete header
fprintf('==== Running completed for dataset 1 (input1.txt) =====\n\n')
unloadlibrary dll

%%%%%%%%%%%%%%%%%% input2.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
%libfunctions dll %List all functions in dll
fprintf('*** Running algorithm for dataset 2 (input2.txt) ***\n')
inputSize2= calllib('dll', 'FileSize', 'input2.txt');
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input2.txt', 'compressed2')
fprintf('*** Compression Completed!!!\n');
fprintf('Input File Size: %d Bytes\n', inputSize2);
compressedSize2= calllib('dll', 'FileSize', 'compressed2');
fprintf('Compressed File Size: %d Bytes\n', compressedSize2);
cr2= calllib('dll', 'CR', 'input2.txt', 'compressed2');
sp2= calllib('dll', 'SavingPercentage', cr2);
fprintf('Compression Ratio: %.2f\n', cr2);
fprintf('Saving Percentage: %.2f\n', sp2);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed2', 'encrypted2')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted2', 'decrypted2')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted2', 'output2.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize2= calllib('dll', 'FileSize', 'output2.txt');
fprintf('Output File Size: %d Bytes\n', outputSize2);
delete header
fprintf('==== Running completed for dataset 2 (input2.txt) =====\n\n')
unloadlibrary dll

%%%%%%%%%%%%%%%%%% input3.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
%libfunctions dll %List all functions in dll
fprintf('*** Running algorithm for dataset 3 (input3.txt) ***\n')
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input3.txt', 'compressed3')
fprintf('*** Compression Completed!!!\n');
inputSize3= calllib('dll', 'FileSize', 'input3.txt');
fprintf('Input File Size: %d Bytes\n', inputSize3);
compressedSize3= calllib('dll', 'FileSize', 'compressed3');
fprintf('Compressed File Size: %d Bytes\n', compressedSize3);
cr3= calllib('dll', 'CR', 'input3.txt', 'compressed3');
sp3= calllib('dll', 'SavingPercentage', cr3);
fprintf('Compression Ratio: %.2f\n', cr3);
fprintf('Saving Percentage: %.2f\n', sp3);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed3', 'encrypted3')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted3', 'decrypted3')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted3', 'output3.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize3= calllib('dll', 'FileSize', 'output3.txt');
fprintf('Output File Size: %d Bytes\n', outputSize3);
delete header
fprintf('==== Running completed for dataset 3 (input3.txt) =====\n\n')
unloadlibrary dll

%%%%%%%%%%%%%%%%%% input4.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
fprintf('*** Running algorithm for dataset 4 (input4.txt) ***\n')
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input4.txt', 'compressed4')
fprintf('*** Compression Completed!!!\n');
inputSize4= calllib('dll', 'FileSize', 'input4.txt');
fprintf('Input File Size: %d Bytes\n', inputSize4);
compressedSize4= calllib('dll', 'FileSize', 'compressed4');
fprintf('Compressed File Size: %d Bytes\n', compressedSize4);
cr4= calllib('dll', 'CR', 'input4.txt', 'compressed4');
sp4= calllib('dll', 'SavingPercentage', cr4);
fprintf('Compression Ratio: %.2f\n', cr4);
fprintf('Saving Percentage: %.2f\n', sp4);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed4', 'encrypted4')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted4', 'decrypted4')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted4', 'output4.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize4= calllib('dll', 'FileSize', 'output4.txt');
fprintf('Output File Size: %d Bytes\n', outputSize4);
delete header
fprintf('==== Running completed for dataset 4 (input4.txt) =====\n\n')
unloadlibrary dll

%%%%%%%%%%%%%%%%%% input5.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
fprintf('*** Running algorithm for dataset 5 (input5.txt) ***\n')
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input5.txt', 'compressed5')
fprintf('*** Compression Completed!!!\n');
inputSize5= calllib('dll', 'FileSize', 'input5.txt');
fprintf('Input File Size: %d Bytes\n', inputSize5);
compressedSize5= calllib('dll', 'FileSize', 'compressed5');
fprintf('Compressed File Size: %d Bytes\n', compressedSize5);
cr5= calllib('dll', 'CR', 'input5.txt', 'compressed5');
sp5= calllib('dll', 'SavingPercentage', cr5);
fprintf('Compression Ratio: %.2f\n', cr5);
fprintf('Saving Percentage: %.2f\n', sp5);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed5', 'encrypted5')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted5', 'decrypted5')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted5', 'output5.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize5= calllib('dll', 'FileSize', 'output5.txt');
fprintf('Output File Size: %d Bytes\n', outputSize5);
delete header
fprintf('==== Running completed for dataset 5 (input5.txt) =====\n\n')
unloadlibrary dll

%%%%%%%%%%%%%%%%%% input6.txt
libisloaded('dll')
loadlibrary ('dll','main.h')
libisloaded('dll')
fprintf('*** Running algorithm for dataset 6 (input6.txt) ***\n')
fprintf('Compression Started ...\n');
calllib('dll', 'CompressionWithBitStuffingAndHuffman', 'input6.txt', 'compressed6')
fprintf('*** Compression Completed!!!\n');
inputSize6= calllib('dll', 'FileSize', 'input6.txt');
fprintf('Input File Size: %d Bytes\n', inputSize6);
compressedSize6= calllib('dll', 'FileSize', 'compressed6');
fprintf('Compressed File Size: %d Bytes\n', compressedSize6);
cr6= calllib('dll', 'CR', 'input6.txt', 'compressed6');
sp6= calllib('dll', 'SavingPercentage', cr6);
fprintf('Compression Ratio: %.2f\n', cr6);
fprintf('Saving Percentage: %.2f\n', sp6);
fprintf('**** Encryption Started ...\n');
calllib('dll', 'encryptFileWithHamming', 'compressed6', 'encrypted6')
fprintf('*** Encryption Completed!!!\n');
fprintf('**** Decryption Started ...\n');
calllib('dll', 'decryptFileWithHamming', 'encrypted6', 'decrypted6')
fprintf('*** Decryption Completed!!!\n');
fprintf('**** Decompression Started ...\n');
calllib('dll', 'DecompressionWithBitStuffingAndHuffman', 'decrypted6', 'output6.txt')
fprintf('*** Decompression Completed!!!\n');
outputSize6= calllib('dll', 'FileSize', 'output6.txt');
fprintf('Output File Size: %d Bytes\n', outputSize6);
delete header
fprintf('==== Running completed for dataset 6 (input6.txt) =====\n\n')
unloadlibrary dll


%%% PLOTTING %%%
% x=[inputSize inputSize1 inputSize2 inputSize3 inputSize4 inputSize5];
% y= [compressedSize compressedSize1 compressedSize2 compressedSize3 compressedSize4 compressedSize5];
x=[inputSize1 inputSize2 inputSize3 inputSize4 inputSize5 inputSize6];
y= [compressedSize1 compressedSize2 compressedSize3 compressedSize4 compressedSize5 compressedSize6];
spArray= [sp1, sp2, sp3, sp4, sp5, sp6];
x=x./1000; % in KB
y=y./1000; % in KB

% plot(x,sp);
% title('File Size VS Saving Percentage')
% xlabel('Input File Size')
ylabel('File Size in KB')
a= [x(1) y(1); x(2) y(2); x(3) y(3); x(4) y(4); x(5) y(5); x(6) y(6)];
bar(a)

