#ifndef _H_MAIN
#define _H_MAIN
#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "shrhelp.h"
#include <math.h>


/* number of uncoded data bits and data values */
#define DATA_BITS       4
#define DATA_VALUES     (1 << DATA_BITS)

/* number of parity bits and data values */
#define PARITY_BITS     3
#define PARITY_VALUES   (1 << PARITY_BITS)

/* number of code bits (data + parity) and data values */
#define CODE_BITS       (DATA_BITS + PARITY_BITS)
#define CODE_VALUES     (1 << CODE_BITS)

/* code matrix 2nd, 3rd, and 4th MSB define parity bits */
const unsigned char g[DATA_BITS] =
{
    0x38,   /* 0 1 1 | 1 0 0 0 */
    0x54,   /* 1 0 1 | 0 1 0 0 */
    0x62,   /* 1 1 0 | 0 0 1 0 */
    0x71    /* 1 1 1 | 0 0 0 1 */
};

/* code matrix trasposed for ease of use in routines */
const unsigned char gT[CODE_BITS] =
{
    0x07,   /* 0 1 1 1 */
    0x0B,   /* 1 0 1 1 */
    0x0D,   /* 1 1 0 1 */
            /* ------- */
    0x08,   /* 1 0 0 0 */
    0x04,   /* 0 1 0 0 */
    0x02,   /* 0 0 1 0 */
    0x01    /* 0 0 0 1 */
};
/* parity check matrix.  4 LSBs indicate which bits a parity bit represents */
const unsigned char h[PARITY_BITS] =
{
    0x47,   /* 1 0 0 | 0 1 1 1 */
    0x2B,   /* 0 1 0 | 1 0 1 1 */
    0x1D    /* 0 0 1 | 1 1 0 1 */
};

/* convert syndrome (H x data) to mask that corrects data error */
const unsigned char syndromeMask[PARITY_VALUES] =
{
    0x00,   /* syndrome = 0 0 0 */
    0x10,   /* syndrome = 0 0 1 */
    0x20,   /* syndrome = 0 1 0 */
    0x08,   /* syndrome = 0 1 1 */
    0x40,   /* syndrome = 1 0 0 */
    0x04,   /* syndrome = 1 0 1 */
    0x02,   /* syndrome = 1 1 0 */
    0x01    /* syndrome = 1 1 1 */
};


#define CHAR_RANGE  257     /* max number of character values */
#define FAKE_EOF    256     /* special value to signify end-of-file */
#define CHAR_BITS   8

/* Huffman tree structure */
typedef struct htree
{
    struct htree *left;
    struct htree *right;
    int letter;
    int freq;
} htree;

/* compute left justified 7 bit Hamming code from 4 bit data */
EXPORTED_FUNCTION unsigned char HammingMatrixEncode(unsigned char data);
/* compute left justified 4 bit data value from 7 bit Hamming code */
EXPORTED_FUNCTION unsigned char HammingMatrixDecode(unsigned char code);


/* Utilities */
EXPORTED_FUNCTION int readTxtFile(char *fileName, char **dataRead);
EXPORTED_FUNCTION int writeTxtFile(char *fileName, char *dataWrite);
EXPORTED_FUNCTION int writeBinaryFile(const char *fileName, char *dataToWrite);
EXPORTED_FUNCTION long FileSize(char *in);
EXPORTED_FUNCTION double CR(char *inFileName, char *compressedFileName);
EXPORTED_FUNCTION double SavingPercentage(double CR);


EXPORTED_FUNCTION void BitCompression(char *in, char *out);
EXPORTED_FUNCTION void BitDecompression(char *in, char *out);

EXPORTED_FUNCTION void HuffmanCompression(char *inFileName, char *outFileName);
EXPORTED_FUNCTION void HuffmanDecompression(char *inFileName, char *outFileName);

EXPORTED_FUNCTION void encryptFileWithHamming(char *inputFileName, char *encryptedFileName);
EXPORTED_FUNCTION void decryptFileWithHamming(char *encryptedFileName, char *decryptedFileName);


/* Main algorithm */
EXPORTED_FUNCTION void CompressionWithBitStuffingAndHuffman(char *pInFileName, char *pCompressedFileName);
EXPORTED_FUNCTION void DecompressionWithBitStuffingAndHuffman(char *pInFileName, char *pOutFileName);








#endif